﻿using UnityEngine;

public class GameMenuController : MonoBehaviour
{
    public GameObject Panel;
    private GameObject _menu;

    void Start()
    {
        if (Panel == null)
            throw new System.Exception("Panel is null");
    }
   
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
            ToggleInventory();
    }

    void ToggleInventory()
    {
        if (_menu == null)
        {
            _menu = Instantiate(Panel);
            _menu.transform.SetParent(transform, false);

        }
        else
            Destroy(_menu);
    }
}
