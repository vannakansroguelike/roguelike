﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextHighlight : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    private Text _text;
    private Color _originalColor;
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual void Initialize()
    {
        _text = GetComponent<Text>();
        _originalColor = _text.color;
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        _text.color = Color.white;
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        _text.color = _originalColor;
    }


    public virtual void OnPointerClick(PointerEventData eventData)
    {
    }
   
}
