﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ExitButtonCommand : TextHighlight
{
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        Application.Quit();
    }
}
