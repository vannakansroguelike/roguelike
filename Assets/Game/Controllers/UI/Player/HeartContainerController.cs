﻿using System.Collections;
using System.Collections.Generic;
using Assets.Game.Controllers.Mechanics.Stats;
using UnityEngine;
using UnityEngine.Events;

public class HeartContainerController : MonoBehaviour
{
    public GameObject HeartPrefab;
    public float Spacing;
    private List<GameObject> HealthObjects;
    private int _currentOffset = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        var playerStats = FindObjectOfType<PlayerStatsController>()?.PlayerStats;
        playerStats.OnHealthChanged += OnHealthChanged;
        HealthObjects = new List<GameObject>();

        AddHeart(playerStats.Stats.Health);
    }


    void OnHealthChanged(StatAction action, int value)
    {
        if (action == StatAction.Addition)
            AddHeart(value);
        else
            DestroyHeart(value);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddHeart(int value)
    {
        for (int i = 0; i < value; i++)
        {
            var heart = Instantiate(HeartPrefab);
            heart.transform.SetParent(this.transform);
            heart.transform.position = this.transform.position;
            heart.transform.position += new Vector3(_currentOffset * Spacing, 0);
            HealthObjects.Add(heart);
            _currentOffset++;
        }
 
    }

    void DestroyHeart(int amount)
    {
        if (amount > HealthObjects.Count)
            return;

        for (int i = 0; i < amount; i++)
        {
            var healthToRemove = HealthObjects[HealthObjects.Count - 1];
            HealthObjects.Remove(healthToRemove);
            Destroy(healthToRemove);
        }
    }
}
