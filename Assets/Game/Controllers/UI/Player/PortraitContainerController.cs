﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortraitContainerController : MonoBehaviour
{
    //Has Animation
    //Logic for random timing of the blinks
    //Yeah buddy


    public Animation Animation;
    public Animator Animator;
    public int BlinkCooldown = 3;
    public float BlinkChance = 0.995f;
    private float _targetTime;
    private bool _isBlinkCooldown;

    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void FixedUpdate()
    {
        if (Time.time >= _targetTime)
            _isBlinkCooldown = false;

        if (!_isBlinkCooldown)
        {
            var random = Random.Range(0f, 1f);
            if (random > BlinkChance)
            {
                _targetTime = Time.time + BlinkCooldown;
                _isBlinkCooldown = true;
                Animator.Play("PlayerPortraitAnimation", -1, 0f);
            }
        }
    }
}
