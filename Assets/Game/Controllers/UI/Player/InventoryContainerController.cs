﻿using UnityEngine;

public class InventoryContainerController : MonoBehaviour
{
    public GameObject InventoryPrefab;
    private GameObject _inventory;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.I))
            ToggleInventory();
    }


    void ToggleInventory()
    {
        if (_inventory == null)
            CreateInventory();
        else
            Destroy(_inventory);
    }


    void CreateInventory()
    {
        _inventory = Instantiate(InventoryPrefab);
        _inventory.transform.SetParent(transform, false);
    }
}
