﻿using System.Collections.Generic;
using Assets.Game.Controllers.Mechanics.Stats;
using UnityEngine;

public class ManaContainerController : MonoBehaviour
{
    public GameObject ManaPrefab;
    public float Spacing;

    private int _currentOffset = 0;
    private List<GameObject> ManaObjects;

    // Start is called before the first frame update
    void Start()
    {
        ManaObjects = new List<GameObject>();

        var playerStats = FindObjectOfType<PlayerStatsController>().PlayerStats;
        playerStats.OnManaChanged += OnManaChanged;

        AddMana(playerStats.Stats.Mana); //rename
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnManaChanged(StatAction action, int value)
    {
        if (action == StatAction.Addition)
            AddMana(value);
        else
            DestroyMana(value);
    }

    void AddMana(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            var mana = Instantiate(ManaPrefab);
            mana.transform.SetParent(transform);
            mana.transform.position = transform.position;
            mana.transform.position += new Vector3(_currentOffset * Spacing, 0);
            ManaObjects.Add(mana);
            _currentOffset++;
        }

    }

    void DestroyMana(int amount)
    {
        if (amount > ManaObjects.Count)
            return;

        for (int i = 0; i < amount; i++)
        {
            var manaToRemove = ManaObjects[ManaObjects.Count - 1];
            ManaObjects.Remove(manaToRemove);
            Destroy(manaToRemove);
        }
    }
}
