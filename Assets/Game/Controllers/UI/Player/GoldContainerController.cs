﻿using Assets.Game.Controllers.Mechanics.Stats;
using UnityEngine;
using UnityEngine.UI;

public class GoldContainerController : MonoBehaviour
{
    private Text GoldText;
    private int  _gold;
    public int Gold { get { return _gold; } set { _gold = value; GoldText.text = $"{_gold}"; } } //This needs to change soon, need ot centralize player stats and use events to update it all

    // Start is called before the first frame update
    void Start()
    {
        GoldText = GetComponentInChildren<Text>();
        var stats = FindObjectOfType<PlayerStatsController>();
        Gold = stats.PlayerStats.Stats.Gold;
        stats.PlayerStats.OnGoldChanged += OnGoldChanged;
    }

    void OnGoldChanged(StatAction action, int value)
    {
        Debug.Log($"Picked up {value} gold");
        if (action == StatAction.Addition)
            Gold += value;
        else
            Gold -= value;
    }
}
