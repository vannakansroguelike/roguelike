﻿using Assets.Game;
using Assets.Game.Controllers.Items.Serializable;
using UnityEngine;

public class PlayerInventoryUIController : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private ItemDatabaseController _itemDatabase;
    private GameObject _player;

    public GameObject Panel;

    public GameObject PanelItemPrefab;

    // Start is called before the first frame update
    private void Start()
    {
        _itemDatabase = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ItemDatabaseController>(); //hmmmmppppp
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _spriteRenderer.enabled = true;
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
            throw new System.Exception("Player is null");
        Debug.Log("UI Inventory Created");

        var playerInventory = _player.GetComponent<PlayerInventoryController>();
        playerInventory.Items.ForEach(AddItemToGrid);

    }

    private void RemoveFromPlayerInventory() //Better name.
    {

    }

    private void AddToPlayerInventory() //Better name.
    {

    }

    private void AddItemToGrid(ItemInfo item)
    {
        //PanelItemCOntroller needs to be more independent
        var newItem = Instantiate(PanelItemPrefab);
        var controller = newItem.GetComponent<PanelItemController>();
        controller.SetItemInfo(item);
        var spriteRenderer = newItem.GetComponent<UnityEngine.UI.Image>();
        var texInfo = _itemDatabase.GetTextureInfo(item.TexId).FileName;
        var tex = new Texture2D(1,1).LoadTexture(texInfo);
        spriteRenderer.overrideSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
        newItem.transform.SetParent(Panel.transform,false);
    }

}
