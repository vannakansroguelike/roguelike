﻿using System.Collections.Generic;
using Assets.Game.Controllers.Items.ItemEffects;
using Assets.Game.Controllers.Items.Serializable;
using Assets.Game.Controllers.Mechanics.Stats;

namespace Assets.Game.Controllers.Items.Model
{
    public class Item
    {
        public List<IItemEffect> ItemEffects { get; set; }
        public Stats Stats { get; set; }
        public ItemInfo ItemInfo { get; set; }
    }
}
