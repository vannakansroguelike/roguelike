﻿namespace Assets.Game.Controllers.Items.Model.Weapon
{
    public enum WeaponType
    {
        Range, Melee
    }
}
