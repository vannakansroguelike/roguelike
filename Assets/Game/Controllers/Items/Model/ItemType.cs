﻿namespace Assets.Game.Controllers.Items.Model
{
    public enum ItemType { Weapon, Armor, Upgrade }
}
