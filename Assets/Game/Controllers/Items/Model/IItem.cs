﻿using System.Collections.Generic;
using Assets.Game.Controllers.Items.ItemEffects;

namespace Assets.Game.Controllers.Items.Model
{
    public interface IItem
    {
        List<IItemEffect> ItemEffects { get; set; }
    }
}
