﻿using Assets.Game;
using Assets.Game.Constants;
using Assets.Game.Controllers.Items;
using System.Collections.Generic;
using Assets.Game.Controllers.Items.Model.Weapon;
using Assets.Game.Controllers.Items.Serializable;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    private List<string> _superlatives;
    private List<string> _adjectives;
    private List<string> _names;
    private List<string> _weaponTypes;
    private List<string> _misc;

    // Start is called before the first frame update
    void Start()
    {
        _superlatives = StreamableAssetFiles.SuperlativeText.LoadTextList();
        _weaponTypes = StreamableAssetFiles.ItemNamesText.LoadTextList();
        _names = StreamableAssetFiles.NamesText.LoadTextList();
    }

    private void GenerateRandomItem()
    {

    }

    private void GenerateItemStats()
    {

    }

    ItemInfo GenerateItem()
    {


        return new ItemInfo() { };
    }

    string GenerateName()
    {
        return "";
    }
}

public enum ItemRequestType { }

public class GenerateItemRequest
{

}
