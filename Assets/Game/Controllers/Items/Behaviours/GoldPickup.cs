﻿using Assets.Game.Controllers.Mechanics.Stats;
using UnityEngine;

namespace Assets.Game.Controllers.Items.Behaviours
{
    public class GoldPickup : InteractableItem
    {
        public int Amount;

        public override void Start()
        {
            IsPickup = true;
            Amount = Random.Range(0, 1000);
        }

        public override void Interact(GameObject target)
        {
            var stats = target.GetComponent<PlayerStatsController>();
            if(stats != null)
                stats.PlayerStats.ChangeStat(Stat.Gold,StatAction.Addition,Amount);

            Destroy(gameObject);
        }
    }
}
