﻿using UnityEngine;

public class FloatingItemController : MonoBehaviour
{
    private float _floatSpeed = 0.00005f;
    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + Mathf.Sin(Time.time) * _floatSpeed);
    }
}
