﻿using Assets.Game.Controllers.Items.ItemEffects;

namespace Assets.Game.Controllers.Items.Effects
{
    public class ItemEffect : IItemEffect
    {
        public int Id { get; set; }

        public virtual void Effect()
        {

        }

        public virtual void ActivateEffect()
        {

        }
    }
}
