﻿namespace Assets.Game.Controllers.Items.ItemEffects
{
    public interface IItemEffect
    {
        int Id { get; set; }
        void Effect();
        void ActivateEffect();
    }
}
