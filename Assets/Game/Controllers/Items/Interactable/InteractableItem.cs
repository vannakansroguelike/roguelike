﻿using UnityEngine;

namespace Assets.Game.Controllers.Items
{
    public class InteractableItem : Interactable.Interactable
    {
        public bool IsPickup { get; set; }
    }
}
