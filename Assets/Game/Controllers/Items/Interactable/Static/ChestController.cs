﻿using Assets.Game.Controllers.Items;
using UnityEngine;

public class ChestController : InteractableItem
{
    public Sprite ClosedSprite;
    public Sprite OpenSprite;
    public Sprite EmptySprite;
    private SpriteRenderer _spriteRenderer;



    public ChestController()
    {
    }

    // Start is called before the first frame update
    public override void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        if (_spriteRenderer == null)
            throw new System.Exception("Sprite renderer is null");

        IsPickup = false;
    }

    // Update is called once per frame
    public override void Update()
    {
        
    }

    public override void Interact(GameObject player)
    {
       // base.Interact(player);
        Debug.Log("Interacted");
        _spriteRenderer.sprite = OpenSprite;
    }

}
