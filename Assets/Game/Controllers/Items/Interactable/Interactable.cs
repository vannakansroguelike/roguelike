﻿using UnityEngine;

namespace Assets.Game.Controllers.Items.Interactable
{
    public class Interactable : MonoBehaviour
    {
        public virtual void Start()
        {

        }


        public virtual void Update()
        {

        }


        public virtual void Interact(GameObject target)
        {

        }
    }
}
