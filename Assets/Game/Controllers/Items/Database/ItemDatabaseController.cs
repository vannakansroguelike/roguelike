﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Game.Controllers.Items.Serializable;
using UnityEngine;

public class ItemDatabaseController : MonoBehaviour //Change to become database controller?? Or are we making a texturedatabasecontroller........ yheah probably latter
{
    private readonly string _filePath = $"{Application.streamingAssetsPath}/inventory.json";
    private readonly string _textureInfoPath = $"{Application.streamingAssetsPath}/textures.json";
    private ItemCollection _itemCollection;
    private TextureCollection _textureCollection;


    public List<TextureInfo> GetTextureInfo() => _textureCollection;

    public TextureInfo GetTextureInfo(int id) => _textureCollection.Textures.FirstOrDefault(e => e.Id == id);

    public List<ItemInfo> GetInventory() => _itemCollection;

    public void SetInventory(List<ItemInfo> items) => _itemCollection.Items = items;

    public void AddItem(ItemInfo itemInfo) => _itemCollection.Items.Add(itemInfo);

    public void RemoveItem(int id) => _itemCollection.Items.RemoveAll(i => i.Id == id);

    // Start is called before the first frame update
    void Start()
    {
       
    }

    void Awake()
    {
        LoadInventory();
        LoadTextureInfo();
    }

    void OnDestroy()
    {
        SaveInventory();
    }

    public void SaveInventory()
    {
        var json = JsonUtility.ToJson(_itemCollection);
        File.WriteAllText(_filePath,json);
    }

    private void LoadInventory()
    {
        var json = File.ReadAllText(_filePath);
        _itemCollection = JsonUtility.FromJson<ItemCollection>(json);
    }

    private void LoadTextureInfo()
    {
        var json = File.ReadAllText(_textureInfoPath);
        _textureCollection = JsonUtility.FromJson<TextureCollection>(json);
    }

}