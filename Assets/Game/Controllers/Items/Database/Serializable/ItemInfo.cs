﻿using System;

namespace Assets.Game.Controllers.Items.Serializable
{

    [Serializable]
    public class ItemInfo : DatabaseItem
    {
        public int TexId;
    }
}