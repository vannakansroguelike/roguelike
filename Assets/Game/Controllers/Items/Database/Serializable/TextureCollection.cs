﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Controllers.Items.Serializable
{
    [Serializable]
    public class TextureCollection
    {
        [SerializeField]
        public List<TextureInfo> Textures;

        public static implicit operator List<TextureInfo>(TextureCollection i) => i.Textures;
    }
}
