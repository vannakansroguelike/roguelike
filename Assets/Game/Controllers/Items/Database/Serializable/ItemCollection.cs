﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Controllers.Items.Serializable
{
    [Serializable]
    public class ItemCollection
    {
        [SerializeField]
        public List<ItemInfo> Items;

        public static implicit operator List<ItemInfo>(ItemCollection i) => i.Items;
    }
}