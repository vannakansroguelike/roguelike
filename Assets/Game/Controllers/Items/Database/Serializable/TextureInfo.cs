﻿using System;

namespace Assets.Game.Controllers.Items.Serializable
{
    [Serializable]
    public class TextureInfo : DatabaseItem
    {
        public string FileName;
    }

}
