﻿using System;

namespace Assets.Game.Controllers.Items.Serializable
{
    [Serializable]
    public class DatabaseItem
    {
        public int Id;
        public string Name;
    }

}
