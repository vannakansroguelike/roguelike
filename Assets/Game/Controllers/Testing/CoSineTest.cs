﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoSineTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position= new Vector2( Mathf.Cos(Time.time) *0.5f,   Mathf.Sin(Time.time) * 0.5f);
    }
}
