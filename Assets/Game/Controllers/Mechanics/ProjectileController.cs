﻿using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    //Maybe some projectiles will want more info, optional I guess.. maybe add a set target method 
    private GameObject _target;
    private Vector2 _targetVector;
    public float MoveSpeed = 0.3f;
    private float tempMoveSpeed = 0.8f;
    private Rigidbody2D _rigidbody2D;
    private float timer = 10f;
    private float deathtime;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        //  _rigidbody2D.AddForce(_targetVector * MoveSpeed,ForceMode2D.Impulse);
        _rigidbody2D.velocity += _targetVector * tempMoveSpeed;
        transform.rotation = Quaternion.LookRotation(transform.forward, _targetVector);
        deathtime = Time.time + timer;
      
    }

  public void Initialize(GameObject target, Vector3 startingPos)
    {
        transform.position = startingPos;
        _target = target;
    }

    public void Initialize(Vector3 targetVector, Vector3 startingPos)
    {
        transform.position = startingPos;
        _targetVector = targetVector;
    }
    
    
    void Update()
    {
        if (Time.time >= deathtime)
            Destroy(gameObject);
    }
    private void FixedUpdate()
    {
        timer--;
        
      //  _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _rigidbody2D.velocity.y + 0.002f);
    }
}
