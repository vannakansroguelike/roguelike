﻿using System;

namespace Assets.Game.Controllers.Mechanics.Stats
{
    public class PlayerStats
    {

        public Stats Stats;

        public PlayerStats()
        {
            Stats = new Stats()
            {
                Gold = 0,
                Health = 3,
                Mana = 3
            };
        }

        public void Initialize()
        {

        }

        public delegate void OnStatsInitDelegate();

        public delegate void OnStatChangedDelegate(StatAction statAction, int value);

        public event OnStatsInitDelegate OnStatsInit;

        public event OnStatChangedDelegate OnHealthChanged;
        public event OnStatChangedDelegate OnManaChanged;
        public event OnStatChangedDelegate OnGoldChanged;
        public event OnStatChangedDelegate OnSpeedChanged;
        public event OnStatChangedDelegate OnIntellectChanged;
        public event OnStatChangedDelegate OnStrengthChanged;
        public event OnStatChangedDelegate OnDexterityChanged;

        public void ChangeStat(Stat stat, StatAction statAction, int value)
        {
            switch (stat)
            {
                case Stat.Health:
                    OnHealthChanged?.Invoke(statAction, value);
                    break;
                case Stat.Mana:
                    OnManaChanged?.Invoke(statAction, value);
                    break;
                case Stat.Gold:
                    OnGoldChanged?.Invoke(statAction, value);
                    break;
                case Stat.Dexterity:
                    OnDexterityChanged?.Invoke(statAction, value);
                    break;
                case Stat.Intellect:
                    OnIntellectChanged?.Invoke(statAction, value);
                    break;
                case Stat.Speed:
                    OnSpeedChanged?.Invoke(statAction, value);
                    break;
                case Stat.Strength:
                    OnStrengthChanged?.Invoke(statAction, value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(stat), stat, "Stat not recognized");
            }
        }
    }
}
