﻿namespace Assets.Game.Controllers.Mechanics.Stats
{

    public enum Stat { Health, Gold, Mana, Speed, Intellect, Strength, Dexterity }
    public enum StatAction { Addition, Subtraction }

    public struct Stats
    {
        public int Gold;
        public int Health;
        public int Mana;
        public int Speed;
        public int Intellect;
        public int Strength;
        public int Dexterity;
    }
}
