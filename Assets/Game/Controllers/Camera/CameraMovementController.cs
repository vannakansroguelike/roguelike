﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementController : MonoBehaviour
{
   public GameObject Target;
    public float Speed = 2f;
    // Start is called before the first frame update
    void Start()
    {
        if (Target == null)
            throw new MissingReferenceException("Missing Target");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {



        var camX = Mathf.Lerp(transform.position.x, Target.transform.position.x, Time.fixedDeltaTime * Speed);
        var camY = Mathf.Lerp(transform.position.y, Target.transform.position.y, Time.fixedDeltaTime * Speed);
        Vector3 cameraTransform = new Vector3(camX, camY, transform.position.z);


        transform.position = cameraTransform;

    }
}
