﻿using UnityEngine;

public enum PlayerState { Idle = 0, Attack}
public enum Direction { Up =0,Left,Right,Down}

public class PlayerAnimationController : MonoBehaviour
{
    public Animator Animator;

    public float MoveX { set => Animator.SetFloat("MoveX",value); }
    public float MoveY { set => Animator.SetFloat("MoveY", value); }
    public float AttackSpeedModifier { get => Animator.GetFloat("AttackSpeed"); set => Animator.SetFloat("AttackSpeed", value); }
    
 
    // Start is called before the first frame update
    void Start()
    {
        AttackSpeedModifier = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        SetDirectionAnimation();
    }

    void SetDirectionAnimation()
    {
        var left = Input.GetKey(KeyCode.A);
        var right = Input.GetKey(KeyCode.D);
        var up = Input.GetKey(KeyCode.W);
        var down = Input.GetKey(KeyCode.S);

        if (left)
        {
            Animator.SetInteger("Direction", (int)Direction.Left);
            return;
        }
        if (right)
        {
            Animator.SetInteger("Direction", (int)Direction.Right);
            return;
        }

        if (up)
        {
            Animator.SetInteger("Direction", (int)Direction.Up);
            return;
        }
        if (down)
        {
            Animator.SetInteger("Direction", (int)Direction.Down);
            return;
        }


    }

    public void Attack()
    {
        AttackSpeedModifier += 0.01f;
        Animator.SetTrigger("Attack");
        Debug.Log("Attack");
    }

    public void Idle()
    {
        Animator.SetTrigger("Idle");
    }

}
