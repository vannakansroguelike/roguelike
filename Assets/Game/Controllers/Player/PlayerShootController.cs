﻿using UnityEngine;

public enum PlayerDirection  {Left,Right,Up,Down}

public class PlayerShootController : MonoBehaviour
{
    //how do we configure the projectile
    public GameObject projectilePrefab;

    private PlayerDirection _direction;
    private  Rigidbody2D _rigidbody2d;
    private Vector2 _lastVelocity;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2d = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {


        if(Input.GetKeyDown(KeyCode.W))
        {
            _direction = PlayerDirection.Up;
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            _direction = PlayerDirection.Left;
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            _direction = PlayerDirection.Down;
        }

        if(Input.GetKeyDown(KeyCode.D))
        {
            _direction = PlayerDirection.Right;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            gameObject.GetComponent<PlayerAnimationController>().Attack();
            var projectile = Instantiate(projectilePrefab);

            projectile.GetComponent<ProjectileController>().Initialize(_rigidbody2d.velocity != Vector2.zero ? _rigidbody2d.velocity.normalized : _lastVelocity.normalized, transform.position);
        }



        _lastVelocity = _rigidbody2d.velocity != Vector2.zero ? _rigidbody2d.velocity : _lastVelocity != Vector2.zero ? _lastVelocity : new Vector2(transform.right.x, transform.right.y);
    }

    Vector2 CalculateDirection()
    {
        var x = 5;
        switch (_direction)
        {
            case PlayerDirection.Up:
                return new Vector2(0, 1);
        }

        return Vector2.zero;


    }
}
