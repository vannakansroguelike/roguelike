﻿using Assets.Game.Controllers.Items;
using UnityEngine;

public class PlayerInteractController : MonoBehaviour
{
    //When F is pressed, interact with a single item in local area
    private InteractableItem _target;

    void Update()
    {
        //check distance from that target, if distance is above a certain threshold, dont target
        if (Input.GetKeyUp(KeyCode.F) && _target != null)
            _target.Interact(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var target = collision.gameObject.GetComponent<InteractableItem>();
        //if _target isnt null and target is closer than _target
            //_target = target

        if (target != null)
            if (target.IsPickup)
                target.Interact(gameObject);
            else 
                _target = target;
    
    }
}

