﻿using System.Collections.Generic;
using Assets.Game.Controllers.Items.Serializable;
using UnityEngine;

public class PlayerInventoryController : MonoBehaviour
{
    public List<ItemInfo> Items;

    // Start is called before the first frame update
    private void Start()
    {
       GetInventory();
    }

    private void Awake()
    {

    }

    private void GetInventory()
    {
        var gameManager = GameObject.FindGameObjectWithTag("GameManager");
        var itemDb = gameManager.GetComponent<ItemDatabaseController>();
        if(itemDb != null)
            Items = itemDb.GetInventory();
    }

    public void AddItem(ItemInfo itemInfo)
    {
        if(itemInfo != null)
            Items.Add(itemInfo);
    }

    public void RemoveItem(int id)
    {
        Items.RemoveAll(i => i.Id == id);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGoldAdded()
    {

    }
}
