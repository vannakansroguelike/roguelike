﻿using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{

    private Rigidbody2D _rb2d;
    private PlayerAnimationController _animController;
    private SpriteRenderer _spriteRenderer;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _animController = GetComponent<PlayerAnimationController>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");


        var localScale = this.gameObject.transform.localScale;
        if (x > 0 && this.gameObject.transform.localScale.x < 0)
        {
            localScale.x *= -1;
            this.gameObject.transform.localScale = localScale;
        }


        if (x < 0 && this.gameObject.transform.localScale.x > 0)
        {
            localScale.x *= -1;
            this.gameObject.transform.localScale = localScale;
        }


        if (_animController)
        {
            _animController.MoveX = x;
            _animController.MoveY = y;

            if (Input.GetKeyDown(KeyCode.Space))
                _animController.Attack();
        }
    }

    private void FixedUpdate()
    {

        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        _rb2d.AddForce(new Vector2(x, y) * Speed);


    }



}

