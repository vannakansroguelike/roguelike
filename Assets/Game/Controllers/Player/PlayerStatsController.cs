﻿using Assets.Game.Controllers.Mechanics.Stats;
using UnityEngine;

public class PlayerStatsController : MonoBehaviour
{
    public PlayerStats PlayerStats;

    private void Awake()
    {
        PlayerStats = new PlayerStats(); //Logic for pulling in existing stats 
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            PlayerStats.ChangeStat(Stat.Health, StatAction.Subtraction, 1);
        if (Input.GetKeyDown(KeyCode.E))
            PlayerStats.ChangeStat(Stat.Mana, StatAction.Subtraction, 1);
    }
}
