﻿using Assets.Game.Constants;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Assets.Game
{
    public static class Utility
    {

        public static void Load(this Texture2D tex, string fileName)
        {
            var file =  File.ReadAllBytes(StreamableAssetFiles.ItemTextureBundle);
            tex.filterMode = FilterMode.Point;
            tex.LoadImage(file);
        }

        public static Texture2D LoadTexture(this Texture2D tex, string fileName)
        {
            var file = File.ReadAllBytes($"{StreamableAssetFiles.ItemTextures}/{fileName}");
            tex.filterMode = FilterMode.Point;
            tex.LoadImage(file);

            return tex;
        }

        public static List<string> ConvertFromCommaToList(this string raw)
        {
            return raw.Replace(System.Environment.NewLine, string.Empty).Split(',').ToList();
        }

        public static List<string> LoadTextList(this string listName)
        {
            return File.ReadAllText(listName).ConvertFromCommaToList();
        }
    }
}
