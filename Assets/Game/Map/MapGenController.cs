﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class MapGenController : MonoBehaviour
{


    public Tilemap TileMap;
    public Sprite Sprite;
    public Sprite WallSprite;
    public Tile Tile;
    public Tile WallTile;

    // Start is called before the first frame update
    void Start()
    {
        Tile.sprite = Sprite;
        WallTile.sprite = WallSprite;
        GenerateTilemap();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GenerateTilemap()
    {
        var map = new int[20, 20];
        var room = new int[5, 5];
        
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {

                    if (i == 0 || j == 0 || i == map.GetLength(0) - 1 || j == map.GetLength(0) - 1)
                        TileMap.SetTile(new Vector3Int(i, j, 0), WallTile);
                    else
                        TileMap.SetTile(new Vector3Int(i, j, 0), Tile);

            }
        }


       // CreateRoom(0, 0, room);

      //  CreateRoom(map.GetLength(0) - 5, map.GetLength(1) - 5, room);

    }


    public void CreateRoom(int xOffset, int yOffset, int[,]room)
    {
        for (int i = 0; i < room.GetLength(0); i++)
        {
            for (int j = 0; j < room.GetLength(1); j++)
            {
                if (i == 0 || j == 0 || i == room.GetLength(0) - 1 || j == room.GetLength(0) - 1)
                {
                    TileMap.SetTile(new Vector3Int(i + xOffset, j + yOffset, 0), WallTile);
                }
            }
        }
    }
}
