﻿using Assets.Game.Controllers.Items.Serializable;
using UnityEngine;
using UnityEngine.EventSystems;

public class PanelItemController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    private ItemInfo _itemInfo;

    public GameObject ToolTipPrefab;

    private GameObject _toolTip;

    private Vector3 offset = new Vector3(.1f,.55f,0);
    public void SetItemInfo(ItemInfo itemInfo)
    {
        _itemInfo = itemInfo;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _toolTip = Instantiate(ToolTipPrefab,gameObject.transform.parent.transform.parent);
        _toolTip.transform.position = transform.position - offset;        // eventData.pointerCurrentRaycast.worldPosition - offset;
        _toolTip.transform.SetAsLastSibling();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
       Destroy(_toolTip);
    }
}
