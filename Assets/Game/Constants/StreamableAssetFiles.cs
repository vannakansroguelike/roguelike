﻿using UnityEngine;

namespace Assets.Game.Constants
{
    public class StreamableAssetFiles
    {
        public static string SuperlativeText = $"{Application.streamingAssetsPath}/itemGeneration/superlativeText.txt";
        public static string ItemTextures = $"{Application.streamingAssetsPath}/Items/";
        public static string ItemTextureBundle = $"{Application.streamingAssetsPath}/Items/big_axe.png";
        public static string ItemNamesText = $"{Application.streamingAssetsPath}/itemGeneration/itemTypes.txt";
        public static string NamesText = $"{Application.streamingAssetsPath}/itemGeneration/itemNamesText.txt";
    }
}
